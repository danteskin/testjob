'use strict';

//menu aside init
// require('jquery.mmenu');

var form = require('./libs/form.js');
// var aside_menu = require('./libs/aside_menu.js');

require('jquery-ui/ui/widgets/tabs');
$('#tabs').tabs();


// menu for testJob
function gmobileMenu() { 
  
  if($('.b-header__nav').hasClass('nav-invisible')){
    $('.b-header__nav').slideDown();
    $('.b-header__nav').addClass('nav-visible');
    $('.b-header__nav').removeClass('nav-invisible');
    
  }
  else {
    $('.b-header__nav').slideUp();
    $('.b-header__nav').removeClass('nav-visible');
    $('.b-header__nav').addClass('nav-invisible');
    
  }
  
}

$('.navbar__collapse').click(function(e){
  e.preventDefault();

  gmobileMenu();
});
  
//////////////////////

///////////////////////



// require('magnific-popup');
// $('.js-popup').magnificPopup({
//   type:'inline',
//   midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
// });

$('nav > a').bind("click", function(e){
	var anchor = $(this);
	$('html, body').stop().animate({
	scrollTop: $(anchor.attr('href')).offset().top
	}, 1000);
	e.preventDefault();
});
//commonjs
// var tabs = require('tabs');
//
// //or directly include the script and 'tabs' will be global
//
// // make it tabbable!
// var container=document.querySelector('.tab-container')
// tabs(container);
